package processing.filters;

import processing.ConvolutionKernel;
import processing.Image;
import processing.Matf;

public class OuvertureFilter extends ConvolutionFilter{


    public OuvertureFilter(){
        super();
        ConvolutionKernel kernel = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(kernel);
    }

    public void setVoisinsBrush(Matf voisins){
        kernels.get(0).setCoeffsMatrix(voisins);
    }

    @Override
    /**
     * On part du principe que l'image passée en paramètre est une image déjà seuillée (noir/ blanc)
     * Sinon ne marche pas
     */
    public Image applyFilter(Image img) {
        Image result;

        //Application d'une érosion puis d'une dilatation

        ErosionFilter erosion = new ErosionFilter(kernels.get(0).getCoeffMatrix());
        DilatationFilter dilatation = new DilatationFilter(kernels.get(0).getCoeffMatrix());

        result = dilatation.applyFilter(erosion.applyFilter(img));
        return result;
    }
}
