package processing.filters;

import processing.*;

public class SobelFilter extends ConvolutionFilter {

    public SobelFilter(){
        super();
        ConvolutionKernel sobel_x = new ConvolutionKernel(new Matf(new Float[][]{{-1.f,0.0f,1.f},{-2.f,0.f,2.f},{-1.f,0f,1f}}),4.f);
        ConvolutionKernel sobel_y = new ConvolutionKernel(new Matf(new Float[][]{{-1.f,-2.0f,-1.f},{0.f,0.f,0.f},{1.f,2.f,1.f}}),4.f);
        kernels.add(sobel_x);
        kernels.add(sobel_y);
    }


    public Image applyFilter(Image img){

        int[]img_dims = img.getDimensions();
        Image result = new Image(img_dims);

        ConvolutionKernel kernel = this.kernels.get(1);

        Color buffer_gx, buffer_gy;

        int[] half_dims;
        //for(ConvolutionKernel kernel : filter.kernels)
            for(int i = 0 ; i < img_dims[0]; ++i)
                for (int j = 0 ; j < img_dims[1]; ++j) {

                    buffer_gx = new Color();
                    buffer_gy = new Color();
                    half_dims = kernel.getHalfDims().clone();

                    for (int row = 0; row < kernel.getDimsCoeffMatrix()[0]; ++row){
                        for (int col = 0; col < kernel.getDimsCoeffMatrix()[1]; ++col) {


                            Color voisin_color_gx = img.getPixel(i - half_dims[0], j - half_dims[1]);
                            if(voisin_color_gx == null)
                                voisin_color_gx = new Color(0.f,0.f,0.f);

                            Color voisin_color_gy = img.getPixel(i - half_dims[0], j - half_dims[1]);
                            if(voisin_color_gy == null)
                                voisin_color_gy = new Color(0.f,0.f,0.f);

                            float coefficient_gx = this.kernels.get(0).getCoeffMatrix().get_value(row, col);
                            float coefficient_gy = this.kernels.get(1).getCoeffMatrix().get_value(row, col);

                            voisin_color_gx = voisin_color_gx.scale(coefficient_gx);
                            voisin_color_gy = voisin_color_gy.scale(coefficient_gy);

                            buffer_gx = buffer_gx.add(voisin_color_gx);
                            buffer_gy = buffer_gy.add(voisin_color_gy);



                            --half_dims[1];
                        }
                        --half_dims[0];
                        half_dims[1] = kernel.getHalfDims()[1];
                    }
                    buffer_gx = buffer_gx.scale(kernel.getDivider());
                    buffer_gy = buffer_gy.scale(kernel.getDivider());
                    Color pixelResult = buffer_gx.mult(buffer_gx).add(buffer_gy.mult(buffer_gy));
                    pixelResult.setR((float)Math.sqrt((double)pixelResult.getR()));
                    pixelResult.setG((float)Math.sqrt((double)pixelResult.getG()));
                    pixelResult.setB((float)Math.sqrt((double)pixelResult.getB()));

                    //System.out.println(buffer);
                    result.setPixel(i, j, result.getPixel(i,j).add(pixelResult));
                }
        
        return result;
    }
}
