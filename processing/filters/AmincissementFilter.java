package processing.filters;

import io.ImageIO;
import processing.Color;
import processing.ConvolutionKernel;
import processing.Image;
import processing.Matf;

import java.util.ArrayList;
import java.util.List;

public class AmincissementFilter extends ConvolutionFilter{

    public AmincissementFilter(){
        super();
        ConvolutionKernel kernel = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(kernel);
    }

    public AmincissementFilter(Matf voisins){
        super();
        ConvolutionKernel default_voisins = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(default_voisins);
        setVoisinsBrush(voisins);
    }

    public void setVoisinsBrush(Matf voisins){

        for(int i = 0 ; i < voisins.getDimensions()[0]; ++i)
            for(int j = 0 ; j < voisins.getDimensions()[1]; ++j)
                if(voisins.get_value(i,j) != 0.f && voisins.get_value(i,j) != 1.f)
                    throw new RuntimeException("La matrice de voisinnage n'est pas correcte");

        this.kernels.get(0).setCoeffsMatrix(voisins);
    }

    @Override
    public Image applyFilter(Image img) {
        Image result_passe1 = new Image(img.getDimensions()); //image qui sera à retourner comme résultat


        //Initialisation et instanciation des variables dont on a besoin
        int[] dimensions = img.getDimensions(); //dimensions de l'image

        //1ere passe
        //Parcours de tous les pixels de l'image
        for (int i = 0; i < dimensions[0]; ++i) {
            for (int j = 0; j < dimensions[1]; ++j) {     //Parcours de tous les pixels de l'image

                //Si les conditions sont vérifiées, le pixel est modifié
                if(verifConditionsPasse(img,i,j,1))
                    result_passe1.setPixel(i,j,new Color(0.f));
                else
                    result_passe1.setPixel(i,j, img.getPixel(i,j));

            }
        }

        System.out.println("Fin de passe 1");

        ImageIO.out(result_passe1.compare(ImageIO.in("/home/mstr_jordison/Images/passe1.png")),
            "comparepasse1.png");
        ImageIO.out(result_passe1,"passe1.png");


        Image result_passe2 = new Image(img.getDimensions());

        //2ere passe
        //Parcours de tous les pixels de l'image
        for (int i = 0; i < dimensions[0]; ++i) {
            for (int j = 0; j < dimensions[1]; ++j) {     //Parcours de tous les pixels de l'image

                //Si les conditions sont vérifiées, le pixel est modifié
                if(verifConditionsPasse(result_passe1,i,j,2))
                    result_passe2.setPixel(i,j,new Color(0.f));
                else
                    result_passe2.setPixel(i,j, result_passe1.getPixel(i,j));

            }
        }

        System.out.println("Fin de passe 2");
        ImageIO.out(result_passe2.compare(ImageIO.in("/home/mstr_jordison/Images/passe2.png")),
                "comparepasse2.png");

        return result_passe2;
    }

    private List<Boolean> getVoisinsStatus(Image img, int i, int j){
        List<Boolean> result = new ArrayList<>();
        List<Color> voisins = new ArrayList<>();

        boolean b_temp;

        //Récupération des pixels voisins
        voisins.add(img.getPixel(i+1,j));
        voisins.add(img.getPixel(i+1,j+1));
        voisins.add(img.getPixel(i,j+1));
        voisins.add(img.getPixel(i-1,j+1));
        voisins.add(img.getPixel(i-1,j));
        voisins.add(img.getPixel(i-1,j-1));
        voisins.add(img.getPixel(i,j-1));
        voisins.add(img.getPixel(i+1,j-1));
        
        for(int k = 0 ; k < 8 ; ++k){
            result.add(voisins.get(k)!= null && voisins.get(k).getR() == 0.f);
        }

        if(i == 16 && j == 11)
            System.out.println("Problème !");

        return result;
    }

    private boolean verifConditionsPasse(Image img, int i, int j, int num_passe){

        boolean cond1, cond2, cond3, cond4, cond5, cond6;

        cond1 = cond2 = cond3 = cond4 = cond5 = cond6 = true;

        //1ere condition
        if(img.getPixel(i,j).getR()==0.f)
            cond1 = false;

        List<Boolean> voisins_status = getVoisinsStatus(img,i,j);

        //2e condition : nombre de voisins == 1.f >=2
        int cpt = 0;
        for(int ind = 0 ; ind < voisins_status.size(); ++ind)
            if(!voisins_status.get(ind))
                cpt++;
        if(cpt < 2)
            cond2 = false;

        //3e condition : nbre de voisins == 1.f <=6
        if(cpt > 6)
            cond3 = false;


        //4e condition : nbre de transition false->true ==1
        cpt = 0;
        for(int ind = 0 ; ind < voisins_status.size(); ++ind){
            if(ind != 7) {
                if ((voisins_status.get(ind) == false) && (voisins_status.get(ind + 1) == true))
                    cpt++;
            }
            else
                if((voisins_status.get(ind) == false) && (voisins_status.get(0) == true))
                    cpt++;
        }
        if(cpt != 1)
            cond4 = false;


        //Changements conditions 5 et 6 pour la première passe
        if(num_passe == 1) {
            //5e condition : v0 || v2 || v4
            if (!(voisins_status.get(0) || voisins_status.get(2) || voisins_status.get(4)))
                cond5 = false;

            //6e condition : v2 || v4 || v6
            if (!(voisins_status.get(2) || voisins_status.get(4) || voisins_status.get(6)))
                cond6 = false;
        }

        //Changement conditions 5 et 6 pour la seconde passe
        if(num_passe == 2){
            //5e condition : v0 || v2 || v4
            if (!(voisins_status.get(0) || voisins_status.get(2) || voisins_status.get(6)))
                cond5 = false;

            //6e condition : v2 || v4 || v6
            if (!(voisins_status.get(0) || voisins_status.get(4) || voisins_status.get(6)))
                cond6 = false;
        }

        boolean result = cond1 && cond2 && cond3 && cond4 && cond5 && cond6;

        if(i == 16 && j == 11)
            System.out.println("Problème !"+img.getPixel(i,j).getR());

        return result;
    }
}
