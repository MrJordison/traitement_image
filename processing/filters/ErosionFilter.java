package processing.filters;

import processing.Color;
import processing.ConvolutionKernel;
import processing.Image;
import processing.Matf;

public class ErosionFilter extends ConvolutionFilter {


    public ErosionFilter(){
        super();
        ConvolutionKernel default_voisins = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(default_voisins);
    }

    public ErosionFilter(Matf voisins){
        super();
        ConvolutionKernel default_voisins = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(default_voisins);
        setVoisinsBrush(voisins);
    }

    public void setVoisinsBrush(Matf voisins){

        for(int i = 0 ; i < voisins.getDimensions()[0]; ++i)
            for(int j = 0 ; j < voisins.getDimensions()[1]; ++j)
                if(voisins.get_value(i,j) != 0.f && voisins.get_value(i,j) != 1.f)
                    throw new RuntimeException("La matrice de voisinnage n'est pas correcte");

        this.kernels.get(0).setCoeffsMatrix(voisins);


    }


    @Override
    /**
     * On part du principe que l'image passée en paramètre est une image déjà seuillée (noir/ blanc)
     * Sinon ne marche pas
     */
    public Image applyFilter(Image img) {

        Image result = new Image(img.getDimensions()); //image qui sera à retourner comme résultat


        //Initialisation et instanciation des variables dont on a besoin

        //matrice de convolution des voisins qui nous intéresse pour l'érosion
        Matf voisins = kernels.get(0).getCoeffMatrix();


        int[] dimensions = img.getDimensions(); //dimensions de l'image
        int[] voisins_dims = voisins.getDimensions(); //dimensions de la zone de voisins à vérifier
        int[] half_dims = kernels.get(0).getHalfDims().clone(); // demies dimensions de la zone de voisins
        half_dims[0] = -half_dims[0];
        half_dims[1] = -half_dims[1];

        int ibrush, jbrush; // index pour itérer dans la zone de voisins

        boolean hasBlackVoisin; // indicateur si tous les voisins
        Color pixel_temp; // buffer pour récuper un pixel voisin depuis l'image passée en paramètre

        //Parcours de tous les pixels de l'image
        for(int i = 0 ; i < dimensions[0]; ++i){
            for(int j = 0 ; j < dimensions[1]; ++j){

                hasBlackVoisin = false;

                ibrush = jbrush = 0;

                //parcours de la zone de voisins
                while(ibrush < voisins_dims[0] && !hasBlackVoisin){
                    while(jbrush < voisins_dims[1] && !hasBlackVoisin){


                        //Si le voisin est utile (valeur dans brush == 1.f)
                        if(voisins.get_value(ibrush,jbrush) == 1.f){

                            //on récupère la couleur du pixel voisin (égal à 0.f ou 1.f)
                            pixel_temp = img.getPixel(i + (ibrush + half_dims[0]), j + (jbrush + half_dims[1]));

                            //si ce pixel n'est pas en dehors des limites de l'image
                            //si c'est le cas (cad i < 0 || i >= dimensions[1] || j < 0 || j >= dimensions[1]),
                            //la fonction getPixel retourne null
                            if(pixel_temp != null){
                                hasBlackVoisin |= pixel_temp.getR() == 0.f;
                            }
                        }

                        ++jbrush;
                    }
                    ++ibrush;
                    jbrush = 0;
                }

                //Si le pixel en cours à un voisin noir, alors on le met à noir pour l'érosion
                if(!hasBlackVoisin)
                    result.setPixel(i,j,new Color(1.f));
            }
        }

        return result;
    }
}
