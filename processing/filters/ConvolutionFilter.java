package processing.filters;

import processing.ConvolutionKernel;
import processing.filters.Filter;

import java.util.ArrayList;
import java.util.List;

public abstract class ConvolutionFilter implements Filter {

    protected List<ConvolutionKernel> kernels;

    public ConvolutionFilter(){kernels = new ArrayList<>();}

    public ConvolutionFilter(List<ConvolutionKernel> kernels){
        this.kernels = kernels;
    }

    public List<ConvolutionKernel> getKernels(){return kernels;}

    public void addKernel(ConvolutionKernel kernel){
        if(kernels == null)
            kernels = new ArrayList<>();
        kernels.add(kernel);
    }


}
