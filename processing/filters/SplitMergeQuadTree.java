package processing.filters;

import processing.Color;
import processing.Image;

import java.util.ArrayList;
import java.util.List;

public class SplitMergeQuadTree {

    public static final int QUADTREE_MAX_RECURSIVE = 15;

    SplitMergeQuadTree parent;
    List<SplitMergeQuadTree> children;
    Image img;
    int[] min;
    int[] max;
    float delta;
    int position;
    Color moyenne;


    public SplitMergeQuadTree(Image img){
        this.parent = null;
        this.img = img;
        min = new int[]{0,0};
        max = img.getDimensions().clone();
        runGeneration();
        position = -1;
        moyenne = null;
    }

    public SplitMergeQuadTree(Image img, float delta){
        this.parent = null;
        this.img = img;
        min = new int[]{0,0};
        max = img.getDimensions().clone();
        this.delta = delta;
        position = -1;
        moyenne = null;

        runGeneration();

    }

    private SplitMergeQuadTree(SplitMergeQuadTree parent, int[] min, int[] max, int position){
        this.parent = parent;
        this.min = min;
        this.max = max;
        this.delta = parent.delta;
        this.img = parent.img;
        this.position = position;
        moyenne = null;

        int depth = this.getDepth();
        System.out.println(depth);

        runGeneration();

    }

    public int getDepth(){
        if(parent == null)
            return 1;
        else{
            return 1 + parent.getDepth();
        }
    }

    public int getAreaPixels(){
        int largeur = max[1] - min[1];
        int hauteur = max[0] - min[0];
        largeur = largeur <=0 ? 1 : largeur;
        hauteur = hauteur <= 0 ? 1 : hauteur;
        return largeur * hauteur;
    }


    public Image drawSplit(){
        Image result = new Image(img.getDimensions());
        drawSplitRecursive(result);
        return result;
    }

    private void drawSplitRecursive(Image result){
        if(children != null)
            for(int i = 0; i < children.size(); ++i)
                children.get(i).drawSplitRecursive(result);
        else{
            for(int i = min[0] ; i < max[0]; ++i)
                for(int j = min[1]; j < max[1]; ++j){
                    result.setPixel(i,j,moyenne);
                }
        }
    }

    public void runGeneration() {
        Color buffer = new Color(0.f);

        int i, j;

        //Calcul de la moyenne d'intensité de la zone d'image définie par min-max
        for (i = min[0]; i < max[0]; ++i) {
            for (j = min[1]; j < max[1]; ++j) {
                buffer = buffer.add(img.getPixel(i, j));
            }
        }
        int nb_pixels = getAreaPixels();
        buffer = buffer.scale(1.f / (float) nb_pixels);

        //Si on a atteint la récursivité maximum, on arrête la subdivision
        if(getDepth() >= QUADTREE_MAX_RECURSIVE) {
            moyenne = buffer;
            System.out.println("Récursivité maximum atteinte");
        }
        else{
            if ((max[0] - min[0]) < 1 || (max[1] - min[1]) < 1)
                moyenne = buffer;
            else {

                i = min[0];
                j = min[1];

                boolean split = false;

                //Comparaison de chaque intensité de pixel avec valeurs du buffer
                while ((i < max[0]) && (!split)) {
                    while (j < max[1] && !split) {

                        Color temp = img.getPixel(i, j);
                        split |= temp.getR() > (buffer.getR() + delta);

                        j++;
                    }
                    j = min[1];
                    i++;
                }

                if (split) {
                    int[] half_dims = new int[]{(max[0] - min[0]) / 2, (max[1] - min[1]) / 2};

                    children = new ArrayList<>();

                    //Quartier haut gauche
                    children.add(new SplitMergeQuadTree(this, min, new int[]{min[0] + half_dims[0], min[1] + half_dims[1]}, 1));
                    //Quartier haut droit
                    children.add(new SplitMergeQuadTree(this, new int[]{min[0], min[1] + half_dims[1]}, new int[]{min[0] + half_dims[0], max[1]}, 2));
                    //Quartier bas gauche
                    children.add(new SplitMergeQuadTree(this, new int[]{min[0] + half_dims[0], min[1]}, new int[]{max[0], min[1] + half_dims[1]}, 3));
                    //Quartier bas droit
                    children.add(new SplitMergeQuadTree(this, new int[]{min[0] + half_dims[0], min[1] + half_dims[1]}, max, 4));
                } else {
                    moyenne = buffer;
                }
            }
        }
    }
}
