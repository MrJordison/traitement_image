package processing.filters;

import processing.Color;
import processing.ConvolutionKernel;
import processing.Image;
import processing.Matf;

public class DilatationFilter extends ConvolutionFilter {


    public DilatationFilter(){
        super();
        ConvolutionKernel default_voisins = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(default_voisins);
    }

    public DilatationFilter(Matf voisins){
        super();
        ConvolutionKernel default_voisins = new ConvolutionKernel(new Matf(new Float[][]{{0.f,1.f,0.f},{1.f,1.f,1.f},{0.f,1.f,0.f}}),1.f);
        kernels.add(default_voisins);
        setVoisinsBrush(voisins);
    }

    public void setVoisinsBrush(Matf voisins){

        for(int i = 0 ; i < voisins.getDimensions()[0]; ++i)
            for(int j = 0 ; j < voisins.getDimensions()[1]; ++j)
                if(voisins.get_value(i,j) != 0.f && voisins.get_value(i,j) != 1.f)
                    throw new RuntimeException("La matrice de voisinnage n'est pas correcte");

        this.kernels.get(0).setCoeffsMatrix(voisins);


    }


    @Override
    /**
     * On part du principe que l'image passée en paramètre est une image déjà seuillée (noir/ blanc)
     * Sinon ne marche pas
     */
    public Image applyFilter(Image img) {

        Image result = new Image(img.getDimensions()); //image qui sera à retourner comme résultat


        //Initialisation et instanciation des variables dont on a besoin

        //matrice de convolution des voisins qui nous intéresse pour la dilatation
        Matf voisins = kernels.get(0).getCoeffMatrix();


        int[] dimensions = img.getDimensions(); //dimensions de l'image
        int[] voisins_dims = voisins.getDimensions(); //dimensions de la zone de voisins à vérifier
        int[] half_dims = kernels.get(0).getHalfDims().clone(); // demies dimensions de la zone de voisins
        half_dims[0] = -half_dims[0];
        half_dims[1] = -half_dims[1];

        int ibrush, jbrush; // index pour itérer dans la zone de voisins

        boolean hasWhiteVoisin; // indicateur l'intersection de l'ensemble des voisins avec la "zone" blanche est non vide
        Color pixel_temp; // buffer pour récuper un pixel voisin depuis l'image passée en paramètre

        //Parcours de tous les pixels de l'image
        for(int i = 0 ; i < dimensions[0]; ++i){
            for(int j = 0 ; j < dimensions[1]; ++j){

                hasWhiteVoisin = false;

                ibrush = jbrush = 0;

                //parcours de la zone de voisins
                while(ibrush < voisins_dims[0] && !hasWhiteVoisin){
                    while(jbrush < voisins_dims[1] && !hasWhiteVoisin){

                        //Si le coefficient du kernel est différent de 0.f (cad on considère le voisin comme utile)
                        if(voisins.get_value(ibrush, jbrush) != 0.f){

                            //on récupère la couleur du pixel voisin (égal à 0.f ou 1.f)
                            pixel_temp = img.getPixel(i + (ibrush + half_dims[0]), j + (jbrush + half_dims[1]));

                            //si ce pixel n'est pas en dehors des limites de l'image
                            //si c'est le cas (cad i < 0 || i >= dimensions[1] || j < 0 || j >= dimensions[1]),
                            //la fonction getPixel retourne null
                            if(pixel_temp != null)
                                hasWhiteVoisin |= pixel_temp.getR() == 1.;
                        }

                        ++jbrush;
                    }
                    ++ibrush;
                    jbrush = 0;
                }

                //Si le pixel en cours à un voisin blanc, alors on le met à blanc pour la dilatation
                if(hasWhiteVoisin)
                    result.setPixel(i,j,new Color(1.f));
            }
        }

        return result;
    }
}
