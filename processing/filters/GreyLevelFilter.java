package processing.filters;

import processing.Color;
import processing.Image;

public class GreyLevelFilter implements Filter{

    public static final int MOYENNE_PONDEREE_GRIS = 1;
    public static final int MOYENNE_SIMPLE_GRIS = 0;

    private static final Color ponderate_value = new Color(0.2126f,0.7152f,0.0722f);

    private int mode;

    private void setGreyMode(int mode){
        if(mode == MOYENNE_PONDEREE_GRIS)
            this.mode = mode;
        if(mode == MOYENNE_SIMPLE_GRIS)
            this.mode = mode;
        else
            System.out.println("Mode non reconnu");
            this.mode = MOYENNE_PONDEREE_GRIS;
    }

    public GreyLevelFilter(){
        this.mode = MOYENNE_PONDEREE_GRIS;
    }

    public GreyLevelFilter(int mode){
        setGreyMode(mode);
    }

    @Override
    public Image applyFilter(Image img) {
        int[] dimensions = img.getDimensions();
        Image result = new Image(dimensions);

        float value;
        Color temp;
        for(int i = 0 ; i < dimensions[0]; ++i)
            for(int j = 0 ; j < dimensions[1]; ++j){
                if(mode == MOYENNE_PONDEREE_GRIS)
                    temp = img.getPixel(i,j).mult(ponderate_value);
                else
                    temp = img.getPixel(i,j);

                value = (temp.getR()+temp.getG()+temp.getB()) / 3.f;
                result.setPixel(i,j,new Color(value));
            }
        return result;
    }

}
