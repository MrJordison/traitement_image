package processing.filters;

import processing.Image;

public interface Filter {

    public Image applyFilter(Image img);
}
