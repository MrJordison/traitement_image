package processing.filters;

import processing.Image;

public class SplitMergeFilter implements Filter{

    //Marge maximum acceptée entre les intensités de pixels et la moyenne
    //qui ne force pas au split d'une zone de l'image
    private float delta;

    public SplitMergeFilter(float delta){
        this.delta = delta;
    }


    @Override
    public Image applyFilter(Image img) {

        SplitMergeQuadTree smqt = new SplitMergeQuadTree(new GreyLevelFilter().applyFilter(img), delta);

        Image result = smqt.drawSplit();

        return result;
    }
}
