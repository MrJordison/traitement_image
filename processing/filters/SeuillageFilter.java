package processing.filters;

import processing.Color;
import processing.Image;

public class SeuillageFilter implements Filter {

    public static final int SIMPLE_SEUIL = 0;
    public static final int HYSTERISIS_SEUIL = 1;

    private float max;
    private float min;

    private int mode;

    public SeuillageFilter() {
        min = 0.f;
        min = 1.f;
        mode = SIMPLE_SEUIL;
    }

    public SeuillageFilter(float max) {
        min = 0.f;
        this.max = max;
        mode = SIMPLE_SEUIL;
    }

    public SeuillageFilter(float min, float max) {
        this.min = min;
        this.max = max;
        mode = HYSTERISIS_SEUIL;
    }

    public void setMode(int mode) {
        if (mode == SIMPLE_SEUIL)
            this.mode = mode;
        if (mode == HYSTERISIS_SEUIL)
            this.mode = mode;
        else {
            System.out.println("Erreur mode non reconnu");
            this.mode = SIMPLE_SEUIL;
        }
    }

    @Override
    public Image applyFilter(Image img) {

        int[] dimensions = img.getDimensions();

        Image result = new Image(dimensions);
        GreyLevelFilter filter = new GreyLevelFilter();
        Image values = filter.applyFilter(img);

        float temp;

        //Cas seuillage simple:
        if (mode == SIMPLE_SEUIL) {
            for (int i = 0; i < dimensions[0]; ++i)
                for (int j = 0; j < dimensions[1]; ++j) {

                    temp = values.getPixel(i, j).getR();

                    if (temp > max)
                        result.setPixel(i, j, new Color(1.f));
                    else
                        result.setPixel(i, j, new Color(0.f));
                }

        }
        //Cas seuillage hystéris
        else if(mode == HYSTERISIS_SEUIL){
            for(int passe = 0; passe < 2 ; ++passe){
                for (int i = 0; i < dimensions[0]; ++i)
                    for (int j = 0; j < dimensions[1]; ++j) {

                        temp = values.getPixel(i, j).getR();

                        //1ere passe : traiter uniquement les valeurs n'appartenant pas à l'intervalle
                        // [min,max]
                        if(passe == 0) {
                            if (temp > max)
                                result.setPixel(i, j, new Color(1.f));
                            else
                                result.setPixel(i, j, new Color(0.f));
                        }
                        //2e passe : traiter les cas dans l'intervalle
                        //et les mettre à 1. si voisin d'un pixel à 1.f
                        else if(passe == 1){
                            if(temp != 0.f && temp != 1.f){
                                if(hasContourNeighbour(i,j,result))
                                    result.setPixel(i,j,new Color(1.f));
                                else
                                    result.setPixel(i,j,new Color(0.f));
                            }
                        }
                    }
            }
        }
        return result;
    }

    private boolean hasContourNeighbour(int i, int j, Image result){
        int neighbour_i, neighbour_j, k;

        Color contour = new Color(1.f);
        Color neighbour;
        boolean find = false;
        k = 0;
        while(k < 3 && !find) {
            if (k == 0){
                neighbour_i = -1;
                neighbour_j = 0;
            }
            else if(k == 1) {
                neighbour_i = 1;
                neighbour_j = 0;
            }
            else if(k == 2) {
                neighbour_i = 0;
                neighbour_j = -1;
            }
            else {
                neighbour_i = 0;
                neighbour_j = 1;
            }
            neighbour = result.getPixel(i+neighbour_i, j+neighbour_j);
            if(neighbour == null)
                neighbour = new Color(0.f);
            find = neighbour.equals(contour);

            ++k;
        }
        return find;
    }
}
