package processing;

import java.util.ArrayList;

public class Color extends Matf{

    public Color(){
        super(new int[]{1,3});
    }

    public Color(float r, float g, float b){
        super(new int[]{1,3});
        setR(r);
        setG(g);
        setB(b);
    }

    public Color(float[] color_values){
        super(new int[]{1,3});
        setR(color_values[0]);
        setG(color_values[1]);
        setB(color_values[2]);
    }

    public Color(float value){
        super(new int[]{1,3}, value);
    }

    private Color(Matf m){
        if(m.dimensions[0] != 1 || m.dimensions[1] !=3)
            System.out.println("Erreur dimensions non compatibles avec class Color");
        dimensions = m.dimensions.clone();
        values = new ArrayList<>(m.values);
    }

    public float getR(){return values.get(0);}
    public float getG(){return values.get(1);}
    public float getB(){return values.get(2);}

    public void setR(float r){values.set(0, r);}
    public void setG(float g){values.set(1, g);}
    public void setB(float b){values.set(2, b);}

    public Color getIntensity(){
        Color coeffs =new Color(0.2126f,0.7152f,0.0722f);
        Color temp = this.mult(coeffs);
        float intensity = temp.getR()+temp.getG()+temp.getB();
        intensity /= 3.f;
        return new Color(intensity);
    }

    public Color add(Color c){
        return new Color(super.add((Matf)c));
    }

    public Color sub(Color c){
        return new Color(super.sub((Matf)c));
    }

    public Color mult(Color c){
        float temp;
        float r,g,b;
        temp = getR()*c.getR();
        r = (Float.isNaN(temp) || Float.isInfinite(temp)) ? 0.f : temp;
        temp = getG()*c.getG();
        g = (Float.isNaN(temp) || Float.isInfinite(temp)) ? 0.f : temp;
        temp = getB()*c.getB();
        b = (Float.isNaN(temp) || Float.isInfinite(temp)) ? 0.f : temp;
        return new Color(r,g,b);
    }

    public Color scale(float scalar){
        return new Color(super.scale(scalar));
    }

    public Color add_scalar(float scalar){
        return new Color(super.add_scalar(scalar));
    }

    public Color clone(){
        return new Color(super.clone());
    }

    public Color clamp(float min, float max){
        return new Color(Utils.clamp(getR(),min,max),
            Utils.clamp(getG(),min,max),
            Utils.clamp(getB(),min,max));
    }

    public boolean equals(Color c){
        return (getR() == c.getR() && getG()==c.getG() && getB() == c.getB());
    }
}
