package processing;

public class Utils {
    public static float clamp(float value, float min, float max){
        if(value > max)
            return 1.f;
        if(value < min)
            return 0.f;
        return value;
    }
}
