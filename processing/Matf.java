package processing;

public class Matf extends Matrix<Float>{

    public Matf(){
        super();
    }

    public Matf(int[] dimensions){
        super(dimensions,0.f);
    }


    public Matf(int[] dimensions, float default_value){
        super(dimensions,default_value);
    }

    public Matf(Float[][] values){
        super(values);
    }

    private Matf(Matrix<Float> m){
        this.dimensions = m.dimensions;
        this.values = m.values;
    }

    public Matf add(Matf m){
        if(m.dimensions[0] != dimensions[0] || m.dimensions[1] != dimensions[1])
            return null;

        Matf result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) + m.values.get(i));

        return result;
    }

    public Matf sub(Matf m){
        if(m.dimensions[0] != dimensions[0] || m.dimensions[1] != dimensions[1])
            return null;

        Matf result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) - m.values.get(i));

        return result;
    }

    public Matf mult(Matf m){
        if(dimensions[1] != m.dimensions[0])
            return null;

        Matf result = new Matf(new int[]{dimensions[0], m.dimensions[1]}, 0.f);

        float temp;
        for(int i = 0; i < dimensions[0]; ++i)
            for(int j = 0 ; j < m.dimensions[1]; ++j)
                for(int k = 0 ; k < dimensions[1]; ++k) {
                    temp = get_value(i, k) * m.get_value(j, k);

                    result.set_value(i, j,
                            result.get_value(i, j) + ((Float.isNaN(temp) || Float.isInfinite(temp)) ? 0.f : temp));
                }
        return result;
    }

    public Matf scale(float scalar){
        Matf result = clone();
        float temp;
        for(int i = 0 ; i < values.size(); ++i) {
            temp = result.values.get(i) * scalar;
            result.values.set(i, (Float.isNaN(temp) || Float.isInfinite(temp)) ? 0.f : temp);
        }
        return result;
    }

    public Matf add_scalar(float scalar){
        Matf result = clone();
        for(int i = 0 ; i < values.size(); ++i)
            result.values.set(i, result.values.get(i) + scalar);

        return result;
    }

    public static Matf identity(int[] dimensions){
        Matf result = new Matf(dimensions, 0.f);
        for(int i = 0 ; i < dimensions[0]; ++i)
            result.values.set(i*dimensions[1], 1.f);

        return result;
    }

    public Matf clone(){
        return new Matf(super.clone());
    }

}
