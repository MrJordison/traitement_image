package processing;

import java.util.ArrayList;
import java.util.List;

public class Image{
    private Matrix<Color> pixels_grid;

    public Image(){
        pixels_grid = new Matrix<>();
    }

    public Image(Matrix<Color> grid){
        pixels_grid = grid;
    }

    public Image(int[] dimensions, Color default_color){
        pixels_grid = new Matrix<Color>(dimensions, default_color);
    }

    public Image(int[] dimensions){
        pixels_grid = new Matrix<Color>(dimensions, new Color(0.f));
    }

    public int[] getDimensions(){return pixels_grid.getDimensions();}

    public Color getPixel(int row, int col){return pixels_grid.get_value(row, col);}

    public void setPixel(int row, int col, Color c){pixels_grid.set_value(row, col, c);}

    public Image clone(){return new Image(pixels_grid.clone());}

    public Image compare(Image img){
        Image result = this.clone();
        List<Integer[]> differents = new ArrayList<>();
        for(int i = 0 ; i < getDimensions()[0]; ++i)
            for(int j = 0 ; j < getDimensions()[1]; ++j) {
                if (getPixel(i, j).getR() != img.getPixel(i, j).getR()) {
                    differents.add(new Integer[]{i, j});
                    result.setPixel(i, j, new Color(1.f, 0.f, 0.f));
                }
            }
        if(differents.size()!= 0){
            System.out.println("Nombre de pixels différents : "+differents.size());
            return result;
        }
        else{
            System.out.println("Images identiques");
            return null;
        }
    }
}
