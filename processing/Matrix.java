package processing;

import java.util.ArrayList;
import java.util.List;

public class Matrix<T> {
    protected int[] dimensions;
    protected List<T> values;

    public Matrix() {
        dimensions = new int[]{0, 0};
        values = new ArrayList<T>();
    }

    public Matrix(int[] dimensions) {
        this.dimensions = dimensions;
        values = new ArrayList<T>();
        for(int i = 0; i < dimensions[0] * dimensions[1]; ++i)
            values.add(null);
    }

    public Matrix(int[] dimensions, T default_value) {
        this.dimensions = dimensions;
        values = new ArrayList<T>();
        for (int i = 0; i < dimensions[0] * dimensions[1]; ++i)
            values.add(default_value);
    }

    public Matrix(T[][] values){
        int col = values[0].length;
        for(int i = 1 ; i < values.length; ++i)
            if(values[i].length != col)
                System.out.println("erreur taille de lignes différentes");
        this.dimensions = new int[]{values.length, values[0].length};
        this.values = new ArrayList<T>();
        for(int i = 0 ; i < dimensions[0]; ++i)
            for(int j = 0 ; j < dimensions[1]; ++j)
                this.values.add(values[i][j]);
    }

    public int[] getDimensions(){
        return dimensions;
    }

    public T get_value(int row_index, int column_index) {
        if (row_index < 0){
            //System.out.println("Erreur indice ligne "+row_index+" inférieur à 0");
            return null;
        }
        else if(row_index >= dimensions[0]) {
            //System.out.println("Erreur indice ligne "+row_index+" supérieur ou égal à "+dimensions[0]);
            return null;
        }
        if (column_index < 0){
            //System.out.println("Erreur indice colonne "+column_index+" inférieur à 0");
            return null;
        }
        else if(column_index >= dimensions[1]) {
            //System.out.println("Erreur indice colonne "+column_index+" supérieur ou égal à "+dimensions[1]);
            return null;
        }
        return values.get(row_index * dimensions[1] + column_index);
    }

    public void set_value(int row_index, int column_index, T value) {
        if (row_index < 0 || row_index >= dimensions[0])
            System.out.println("Erreur indice ligne");
        if (column_index < 0 || column_index >= dimensions[1])
            System.out.println("Erreur indice colonne");
        else
            values.set(row_index * dimensions[1] + column_index, value);
    }

    public Matrix<T> clone(){
        Matrix<T> result = new Matrix(this.dimensions);
        result.values = new ArrayList<T>(values);
        return result;
    }

    public String toString(){
        String result = "";
        for(int i = 0 ; i < dimensions[0]; ++i)
            for(int j = 0 ; j < dimensions[1]; ++j)
                result+=get_value(i,j)+" ";
        return result;
    }
}