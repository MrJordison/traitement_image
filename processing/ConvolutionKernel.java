package processing;

public class ConvolutionKernel{
    private Matf matrix;
    private float matrix_divider;

    public ConvolutionKernel(Matf matrix, float divider){
        this.matrix = matrix;
        matrix_divider = divider;
    }

    public int[] getHalfDims(){
        int[] result = matrix.getDimensions().clone();
        result[0] /= 2;
        result[1] /= 2;

        return result;
    }

    public void setCoeffsMatrix(Matf matrix){
        this.matrix = matrix;
    }

    public void setDivider(float divider){
        this.matrix_divider = divider;
    }

    public int[] getDimsCoeffMatrix(){return matrix.getDimensions();}

    public Matf getCoeffMatrix(){return matrix;}

    public float getDivider(){return matrix_divider;}
}
