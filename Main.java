import io.ImageIO;
import processing.*;
import processing.filters.*;

public class Main {

    public static void main(String[] args){
        Image img = ImageIO.in("/home/mstr_jordison/Images/hand.jpg");

        SplitMergeFilter filter = new SplitMergeFilter(0.04f);

        Image result = filter.applyFilter(img);

        ImageIO.out(result, "splitmerge.png");

    }
}