package io;

import processing.Color;
import processing.Image;
import processing.Matrix;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageIO {
    public static Image in(String filename){
        try {
            //Création des instances d'objets nécéssaire au chargement et lecture de l'image source
            BufferedImage imgfile = javax.imageio.ImageIO.read(new File(filename));

            //Récupération des dimensions de l'image pour la création du framebuffer
            int[] dimensions = new int[]{imgfile.getHeight(), imgfile.getWidth()};

            Matrix<Color> framebuffer = new Matrix<Color>(dimensions);


            java.awt.Color c_temp;
            //Création de la matrice de pixels qui est passée comme paramètre à l'instance d'Image
            for (int i = 0; i < dimensions[0]; ++i)
                for (int j = 0; j < dimensions[1]; ++j) {
                    c_temp = new java.awt.Color(imgfile.getRGB(j, i));
                    framebuffer.set_value(i, j, convertToCoreColor(c_temp));
                }

            return new Image(framebuffer);
        }
        catch(IOException e){
            e.printStackTrace();
        }

        return null;
    }

    public static void out(Image image, String filename){
        try{
            int[] dimensions = image.getDimensions();
            BufferedImage imgfile = new BufferedImage(dimensions[1], dimensions[0], BufferedImage.TYPE_INT_RGB);
            java.awt.Color c_temp;
            for(int i = 0 ; i < dimensions[0]; ++i)
                for(int j = 0 ; j < dimensions[1]; ++j){
                    c_temp = convertToJavaColor(image.getPixel(i,j));
                    imgfile.setRGB(j,i,c_temp.getRGB());
                }
            javax.imageio.ImageIO.write(imgfile, "png", new File(filename));
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static Color convertToCoreColor(java.awt.Color c){
        return new Color(c.getRed() / 255.f, c.getGreen() / 255.f, c.getBlue() / 255.f);
    }

    public static java.awt.Color convertToJavaColor(Color c){
        Color cl = c.clamp(0.f,1.f);
        return new java.awt.Color(cl.getR(), cl.getG(), cl.getB());
    }
}
